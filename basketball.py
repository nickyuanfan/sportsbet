import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.linear_model import Ridge


# load the data.
df = pd.read_csv('basketball.csv', names=[
                 'visitor', 'visitor_goals', 'home', 'home_goals'])

import ipdb
ipdb.set_trace()

df['goal_difference'] = df['home_goals'] - df['visitor_goals']
df['home_win'] = np.where(df['goal_difference'] > 0, 1, 0)
df['home_loss'] = np.where(df['goal_difference'] < 0, 1, 0)


df_visitor = pd.get_dummies(df['visitor'], dtype=np.int64)
df_home = pd.get_dummies(df['home'], dtype=np.int64)
# create new variables to show home team win or loss result


# # # subtract home from visitor
df_model = df_home.sub(df_visitor)
df_model['goal_difference'] = df['goal_difference']

# print(df_model)
# df_train = df_model # not required but I like to rename my dataframe with the name train.



lr = Ridge(alpha=0.001)

X = df_model.drop(['goal_difference'], axis=1)
y = df_model['goal_difference']
y = y.fillna(0)

lr.fit(X, y)


df_ratings = pd.DataFrame(data={'team': X.columns, 'rating': lr.coef_})
df_final = df_ratings.sort_values(by=['rating'], ascending=0)
print(df_final)

